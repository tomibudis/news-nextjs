const path = require('path')
const WorkboxPlugin = require('workbox-webpack-plugin')
const withSass = require('@zeit/next-sass')
const getRoutes = require('./routes')
const compose = require('next-compose')
const webpack = require('webpack')
require('dotenv').config()

module.exports = compose([
	[withSass],
	{exportPathMap: getRoutes},
	{
		webpack: (config, { buildId, dev }) => {
		
			/// DotEnv: Environment
			config.plugins.push(
				new webpack.EnvironmentPlugin(process.env)
			)
			const oldEntry = config.entry
			config.entry = () =>
				oldEntry().then(entry => {
					entry['main.js'] &&
						entry['main.js'].push(path.resolve('./utils/offline'))
					return entry
				})
			/* Enable only in Production */
			if (!dev) {
				// Service Worker
				config.plugins.push(
					new WorkboxPlugin.InjectManifest({
						swSrc: path.join(__dirname, 'utils', 'sw.js'),
						swDest: path.join(__dirname, '.next', 'sw.js'),
						globDirectory: __dirname,
						globPatterns: [
							'static/**/*.{png,jpg,ico,eot,svg}' // Precache all static assets by default
						]
					})
				)
			}
	
			return config
		}
	}
])



  


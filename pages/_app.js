import React from 'react'
import Head from 'next/head'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import withRedux from 'next-redux-wrapper'
import initStore from '../utils/store'
import "./../static/styles.global.scss"
import Router from 'next/router';
import AuthService from './../utils/authService';
/* debug to log how the store is being used */
const Auth = new AuthService()

export default withRedux(initStore, {
	debug: typeof window !== 'undefined' && process.env.NODE_ENV !== 'production'
})(
	class MyApp extends App {
		static async getInitialProps({ Component, ctx,_ }) {
			// console.log(!process.browser)
			if(process.browser){
				console.log('browser')
			}
			return {
				pageProps: {
					// Call page-level getInitialProps
					...(Component.getInitialProps
						? await Component.getInitialProps(ctx)
						: {})
				}
			}
		}
		constructor(props) {
			super(props)
			this.state = {
			  isLoading : false
			};
		}
		
		// componentDidMount () {
		// 	if (!Auth.loggedIn()) {
		// 		// this.setState({ isLoading : true})
		// 	  	this.props.router.push('/login')
		// 	}else{
		// 		this.setState({isLoading : false})
		// 	}
		//   }
	
		render() {
			const { Component, pageProps, store } = this.props
			// this.props.router.push('/login')
			return (
				<Container>
					<Head>
						<title>Paynur</title>
					</Head>
					
						<Provider store={store}>
							<Component {...pageProps} />
						</Provider>
					
					
				</Container>
			)
		}
	}
)

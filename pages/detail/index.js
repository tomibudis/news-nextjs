import { Layout } from './../../components';
import Head from 'next/head'
import Populer from './../homepage/populer'
import { User } from 'react-feather'

const Detail = (props) => {
    return(
        <Layout>
            <Head>
                <title>News</title>
            </Head>
            <div className="container">
                <div className="row pt-3">
                    <div className="col-8 pr-1 mb-5 pb-5">
                        <img src="https://colorlib.com/preview/theme/magazine/img/top-post1.jpg" className="img-fluid" />
                        <h4 className="font-bold mt-2">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, ad</h4>
                        12/12/2019 <User size={14}/> Author <span className="badge badge-danger">Travel</span>
                        <p className="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras malesuada sem sed posuere porttitor. Ut sed molestie lectus. Ut posuere laoreet enim, non convallis massa molestie nec. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque egestas pellentesque elementum. Pellentesque et enim non eros sodales feugiat in vel dolor. Praesent molestie finibus sem, sit amet facilisis ligula aliquet sed. Vivamus sodales, risus ut vestibulum luctus, diam enim consectetur metus, a convallis purus augue sed eros. Nulla sed ultrices neque. Nunc finibus dolor orci, a posuere ligula efficitur at. Sed eget varius odio. In hendrerit pharetra sapien at consectetur.

Pellentesque ac est sit amet nulla mattis lobortis quis ac felis. Suspendisse varius magna non ipsum consequat, ut porttitor velit scelerisque. Fusce nec erat viverra, tristique sapien quis, egestas ligula. Phasellus pharetra lacus sit amet diam iaculis, et varius ipsum hendrerit. In hac habitasse platea dictumst. Fusce ac sodales neque, in efficitur tellus. Integer viverra volutpat sapien sed auctor. Aenean in congue nulla. In in placerat dolor, non scelerisque augue. Mauris non odio eget orci condimentum mollis sed vel libero. Ut sodales sapien ac convallis imperdiet. Ut aliquet, urna in cursus facilisis, purus nulla blandit magna, in pharetra elit est a dolor. Sed dignissim rutrum auctor.

Sed tempor augue id convallis vehicula. Mauris laoreet nisi et malesuada eleifend. Pellentesque sed blandit odio. Pellentesque nec magna ultrices, gravida mauris vel, faucibus dui. Aenean accumsan massa luctus lacus vehicula, non finibus dolor convallis. Integer et nisl vel tortor pharetra rutrum quis id orci. Aliquam at lacinia sem. Praesent felis orci, efficitur ut diam in, suscipit pretium elit. Curabitur id tincidunt urna.</p>
                    </div>
                    <div className="col-4">
                        <div className="bg-dark text-white pl-3 py-2">Populer</div>
                        <Populer />
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Detail
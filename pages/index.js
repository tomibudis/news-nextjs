import { compose, renderComponent, renderNothing } from "recompose";

export default function(BaseComponent){
	console.log('auth service')
	return compose(
		branch(
			props => true,
			renderComponent(BaseComponent),
			renderNothing
		)
	)
}
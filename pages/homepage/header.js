import { compose, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { User } from 'react-feather'

const Header = (props) => {
    console.log(props.headlines)
    const { headlines } = props
    return(
        <div className="row mt-2">
            <div className="col-8 pr-2">
                <div className="card border-none box-shadow">
                    { props.headlines.isLoading ? "loading.." : (
                        <div>
                        <div className="card-body p-0">
                            <img src={ headlines.data.articles[0].urlToImage} className="img-fluid" />
                        </div>
                        <div className="card-content-header px-3">
                            <h4><span className="badge badge-warning">
                            {props.headlines.data.articles[0].source.name}
                            </span></h4>
                            <h5 className="font-bold">{ headlines.data.articles[0].title }</h5>
                            <p>{ headlines.data.articles[0].publishedAt } <User size={14} /> {headlines.data.articles[0].author}</p>
                        </div>
                        </div>
                    )}
                    
                </div>
            </div>
            <div className="col-4 pl-0">
                <div className="card border-none box-shadow">
                    { props.headlines.isLoading ? "loading.." : (
                    <div>
                        <div className="card-body p-0">
                            <img src={ headlines.data.articles[1].urlToImage} className="img-fluid" />
                        </div>
                        <div className="card-content-header px-3">
                            <h4><span className="badge badge-warning">
                            {props.headlines.data.articles[1].source.name}
                            </span></h4>
                            <h5 className="font-bold">{ headlines.data.articles[1].title }</h5>
                            <p>{ headlines.data.articles[1].publishedAt } <User size={14} /> {headlines.data.articles[1].author}</p>
                        </div>
                    </div>
                    )}
                </div>
                <div className="card mt-2 border-none box-shadow">
                    { props.headlines.isLoading ? "loading.." : (
                    <div>
                        <div className="card-body p-0">
                            <img src={ headlines.data.articles[2].urlToImage} className="img-fluid" />
                        </div>
                        <div className="card-content-header px-3">
                            <h4><span className="badge badge-warning">
                            {props.headlines.data.articles[2].source.name}
                            </span></h4>
                            <h5 className="font-bold">{ headlines.data.articles[1].title }</h5>
                            <p>{ headlines.data.articles[2].publishedAt } <User size={14} /> {headlines.data.articles[2].author}</p>
                        </div>
                    </div>
                    )}
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return{
        headlines : state.headlines
    }
}
const enhance = compose(
    connect(mapStateToProps),
)

export default enhance(Header)
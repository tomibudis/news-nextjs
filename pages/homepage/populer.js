import { User } from 'react-feather'
const Populer = (props) => {
    return(
        <div>
            <div className="mb-5">
                <div className="row my-3">
                    <div className="col-4">
                        <img src="https://colorlib.com/preview/theme/magazine/img/l2.jpg" className="img-fluid" />
                    </div>
                    <div className="col-8 pl-0">
                        <h5 className="font-bold font-14">Lorem ipsum dolor sit amet</h5>
                        <div className="font-12">12/12/2019 <br />
                        <User size={14}/> Author</div>
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Populer
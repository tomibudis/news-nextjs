import { User } from 'react-feather'
import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { ALL_HEADLINES } from './../../actions/headlines';

const News = (props) => {
    const { headlines } = props
    return(
        <div className="mb-5">
            { headlines.isLoading ? "loading.." : 
                headlines.data.articles.map((data, index)=> (
                    <div className="row my-3" key={index}>
                        <div className="col-4">
                            <img src={data.urlToImage} className="img-fluid" />
                            <div className="card-content-header px-2">
                                <h5><span className="badge badge-danger">{data.source.name}</span></h5>
                            </div>
                        </div>
                        <div className="col-8">
                            <h5 className="font-bold">{ data.title }</h5>
                            {data.publishedAt}
                            <p className="mt-2">
                            {data.description}</p>
                            <User size={14}/> {data.author}
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

const mapStateToProps = state => {
    return {
        headlines : state.headlines
    }
}
const enhance = compose(
    connect(mapStateToProps)
)
export default enhance(News)
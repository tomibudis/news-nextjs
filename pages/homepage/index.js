import { Layout } from './../../components/'
import Head from 'next/head'
import Header from './header'
import News from './news'
import Populer from './populer'
import { compose, lifecycle } from 'recompose'
import { ALL_HEADLINES } from './../../actions/headlines'
import { connect } from 'react-redux'

const Homepage = (props) => {
    return(
        <Layout>
            <Head>
                <title>News</title>
            </Head>
            <div className="container">
                <Header />
                <div className="row mt-3">
                    <div className="col-8 pr-2">
                        <div className="bg-dark text-white pl-3 py-2">Latest New</div>
                        <News />
                    </div>
                    <div className="col-4 pl-0">
                        <div className="bg-dark text-white pl-3 py-2">Populer</div>
                        <Populer />
                    </div>
                </div>
            </div>
        </Layout>
    )
}

const enhance = compose(
    connect(),
    lifecycle({
        componentDidMount() {
            this.props.dispatch( ALL_HEADLINES() )
        }
    })
)
export default enhance(Homepage)
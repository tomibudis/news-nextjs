
# News Test
this News using https://newsapi.org for API.

### Tech on mini project News
- nextjs 
- redux
- bootstrap 4
- dotenv
- recompose
- axios
- redux promise middleware
### How to use

Clone this repository and run npm install, then rename .env.example to .env and fill value.

```sh
$ git clone https://tomibudis@bitbucket.org/tomibudis/news-nextjs.git
$ npm install
```
### Development

```sh
$ npm run dev
```
### Test

```sh
$ npm test
```
### Production

```sh
$ npm run build
and then
$ npm run start
```

import axios from 'axios'

const API_KEY=process.env.API_KEY
const API_URL=process.env.API_URL

export const ALL_HEADLINES = () => {
    return {
        type : "ALL_HEADLINES",
        payload : axios.get(`${API_URL}/top-headlines?country=id&apiKey=${API_KEY}`)

    }
}
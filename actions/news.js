import axios from 'axios'

const API_KEY=process.env.API_KEY
const API_URL=process.env.API_URL

export const ALL_NEWS = () => {
    return {
        type : "ALL_NEWS",
        payload : axios.get(`${API_URL}/everything?q=today&apiKey=${API_KEY}`)

    }
}
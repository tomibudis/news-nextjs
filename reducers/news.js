const initialState = {
    data : [],
    isLoading : true,
    isSuccess : false,
    isError : false
}

const news = (state = initialState, action) => {
    switch(action.type){
        case "ALL_NEWS_PENDING" :
            return {
                ...state,
                isLoading : true,
            }
            break
        case "ALL_NEWS_FULFILLED" : 
            return {
                ...state,
                isLoading : false,
                isSuccess : true,
                data : action.payload.data
            }
            break
        case "ALL_NEWS_REJECTED" :
            return {
                ...state,
                isError :true
            }
        default : 
            return state
    }
}

export default news
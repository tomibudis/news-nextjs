const initialState = {
    data : [],
    isLoading : true,
    isSuccess : false,
    isError : false
}

const headlines = (state = initialState, action) => {
    switch(action.type){
        case "ALL_HEADLINES_PENDING" :
            return {
                ...state,
                isLoading : true,
            }
            break
        case "ALL_HEADLINES_FULFILLED" : 
            return {
                ...state,
                isLoading : false,
                isSuccess : true,
                data : action.payload.data
            }
            break
        case "ALL_HEADLINES_REJECTED" :
            return {
                ...state,
                isError :true
            }
        default : 
            return state
    }
}

export default headlines
import { combineReducers } from 'redux'

import headlines from './headlines'
import news from './news'

export default combineReducers({ 
    headlines,
    news
})

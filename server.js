const { createServer } = require('http')
const path = require('path')
const next = require('next')
const { parse } = require('url');
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dir: '.', dev })
const handle = app.getRequestHandler()
const getRoutes = require('./routes');
const PORT = process.env.PORT || 3000
const routes = getRoutes();
app.prepare().then(_ => {
	const server = createServer((req, res) => {
		const parsedUrl = parse(req.url, true);
		const { pathname, query = {} } = parsedUrl;
		const route = routes[pathname];
		if (req.url === '/sw.js' || req.url.startsWith('/precache-manifest')) {
			app.serveStatic(req, res, path.join(__dirname, '.next', req.url), query)
		} else {
			if (route) {
				return app.render(req, res, route.page, query);
			}
			return handle(req, res)
		}
	})
	
	server.listen(PORT, err => {
		if (err) throw err

		console.log(`> App running on port ${PORT}`)
	})
})

module.exports = () => {
    return {
      '/': { page: '/homepage' },
      '/404': { page: '/404' },
    }
  }
const Footer = () => {
    return(
        <div>
            <div className="footer footer-text" style={{background: "#373737"}}>
                <div className="container font-14 py-2" style={{color: "rgba(255,255,255, .7)"}}>
                    Copyright © 2018 news app. System by tomi budi susilo All rights reserved.
                </div>
            </div>
        </div>
    )
}

export default Footer
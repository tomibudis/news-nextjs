const Navbar = (props) => {
    return(
        <div style={{background: "#373737"}}>
            <div className="container px-0">
                <nav className="navbar navbar-expand-lg navbar-light pb-0" >
                    <a className="navbar-brand text-white" href="#">News App</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                        <li className="nav-item px-2 active">
                            <a className="nav-link text-white" href="#">Home <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item px-2">
                            <a className="nav-link text-white" href="#">Lifestyle</a>
                        </li>
                        <li className="nav-item px-2">
                            <a className="nav-link text-white" href="#">Techno</a>
                        </li>
                        <li className="nav-item px-2">
                            <a className="nav-link text-white" href="#">Travel</a>
                        </li>
                        <li className="nav-item px-2">
                            <a className="nav-link text-white" href="#">Etertaint</a>
                        </li>
                        <li className="nav-item px-2">
                            <a className="nav-link text-white" href="#">Goverment</a>
                        </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export default Navbar
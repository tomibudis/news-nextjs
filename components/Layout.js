import { Navbar, Footer } from './index'

const Layout = (props) => {
    return (
        <div>
            <Navbar />
                <div style={{minHeight:600, backgroundColor: "#F8FAFD"}}>
                    {props.children}
                </div>
            <Footer />
        </div>
    )
}

export default Layout
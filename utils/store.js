import { createStore, compose, applyMiddleware } from 'redux'
import promiseMiddleware from 'redux-promise-middleware'

import rootReducer from '../reducers'

const enhancers = compose(
	typeof window !== 'undefined' && process.env.NODE_ENV !== 'production'
		? window.devToolsExtension && window.devToolsExtension()
		: f => f
)

const createStoreWithMiddleware = applyMiddleware(
	promiseMiddleware()
)(createStore)

export default initialState =>
	createStoreWithMiddleware(rootReducer, initialState, enhancers)

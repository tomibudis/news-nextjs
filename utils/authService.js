import localStorage from 'localStorage'

export default class AuthService {
    login(email, password){
        // get token
        return "login"
    }
    loggedIn(){
        const token = this.getToken()
        return !!token && !isTokenExpired(token)
    }
    setToken(token){
        // Saves user token to localStorage
        localStorage.setItem('token', token)
    }
    
    getToken(){
    // Retrieves the user token from localStorage
        return localStorage.getItem('token')
    }
    logout(){
        localStorage.remove('token')
    }

}